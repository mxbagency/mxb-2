const withImages = require('next-images')
module.exports = withImages({
  images: {
    domains: ['localhost', 'still-forest-77731.herokuapp.com', 'res.cloudinary.com'],
  },
  webpack(config, options) {
    return config
  }
})