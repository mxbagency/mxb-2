import React, { Component } from 'react';
import Link from 'next/link';
import Image from 'next/image';

class ProjectsThreeGrid extends Component {
    render() {
        return (
            <div className="works-area ptb-100">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-4 col-sm-6">
                            <div className="work-card">
                                <Image
                                    alt=''
                                    src='/work/work1.jpg'
                                    width={416}
                                    height={571}
                                />
                                <div className="content">
                                    <span>
                                        <Link href="/projects2#">
                                            <a>Development</a>
                                        </Link>
                                    </span>
                                    <h3>
                                        <Link href="/project-details">
                                            <a>Designing a better cinema experience</a>
                                        </Link>
                                    </h3>

                                    <Link href="/project-details">
                                        <a className="work-btn">Read More</a>
                                    </Link>
                                </div>
                            </div>
                        </div>

                        <div className="col-lg-4 col-sm-6">
                            <div className="work-card">
                                <Image
                                    alt=''
                                    src='/work/work2.jpg'
                                    width={416}
                                    height={571}
                                />

                                <div className="content">
                                    <span>
                                        <Link href="/projects2#">
                                            <a>Web Design</a>
                                        </Link>
                                    </span>
                                    <h3>
                                        <Link href="/project-details">
                                            <a>Building design process within teams</a>
                                        </Link>
                                    </h3>

                                    <Link href="/project-details">
                                        <a className="work-btn">Read More</a>
                                    </Link>
                                </div>
                            </div>
                        </div>

                        <div className="col-lg-4 col-sm-6">
                            <div className="work-card">
                                <Image
                                    alt=''
                                    src='/work/work3.jpg'
                                    width={416}
                                    height={571}
                                />

                                <div className="content">
                                    <span>
                                        <Link href="/projects2#">
                                            <a>eCommerce</a>
                                        </Link>
                                    </span>
                                    <h3>
                                        <Link href="/project-details">
                                            <a>How intercom brings play eCommerce</a>
                                        </Link>
                                    </h3>

                                    <Link href="/project-details">
                                        <a className="work-btn">Read More</a>
                                    </Link>
                                </div>
                            </div>
                        </div>

                        <div className="col-lg-4 col-sm-6">
                            <div className="work-card">
                                <Image
                                    alt=''
                                    src='/work/work4.jpg'
                                    width={416}
                                    height={571}
                                />

                                <div className="content">
                                    <span>
                                        <Link href="/projects2#">
                                            <a>React</a>
                                        </Link>
                                    </span>
                                    <h3>
                                        <Link href="/project-details">
                                            <a>How to start a project with Reactjs</a>
                                        </Link>
                                    </h3>

                                    <Link href="/project-details">
                                        <a className="work-btn">Read More</a>
                                    </Link>
                                </div>
                            </div>
                        </div>

                        <div className="col-lg-4 col-sm-6">
                            <div className="work-card">
                                <Image
                                    alt=''
                                    src='/work/work5.jpg'
                                    width={416}
                                    height={571}
                                />

                                <div className="content">
                                    <span>
                                        <Link href="/projects2#">
                                            <a>Angular</a>
                                        </Link>
                                    </span>
                                    <h3>
                                        <Link href="/project-details">
                                            <a>Examples of different types of sprints</a>
                                        </Link>
                                    </h3>

                                    <Link href="/project-details">
                                        <a className="work-btn">Read More</a>
                                    </Link>
                                </div>
                            </div>
                        </div>

                        <div className="col-lg-4 col-sm-6">
                            <div className="work-card">
                                <Image
                                    alt=''
                                    src='/work/work6.jpg'
                                    width={416}
                                    height={571}
                                />

                                <div className="content">
                                    <span>
                                        <Link href="/projects2#">
                                            <a>Development</a>
                                        </Link>
                                    </span>
                                    <h3>
                                        <Link href="/project-details">
                                            <a>Redesigning the New York times app</a>
                                        </Link>
                                    </h3>

                                    <Link href="/project-details">
                                        <a className="work-btn">Read More</a>
                                    </Link>
                                </div>
                            </div>
                        </div>

                        <div className="col-lg-4 col-sm-6">
                            <div className="work-card">
                                <Image
                                    alt=''
                                    src='/work/work7.jpg'
                                    width={416}
                                    height={571}
                                />

                                <div className="content">
                                    <span>
                                        <Link href="/projects2#">
                                            <a>Graphic Design</a>
                                        </Link>
                                    </span>
                                    <h3>
                                        <Link href="/project-details">
                                            <a>Graphic Design Design the Web, Mobile, and eCommerce</a>
                                        </Link>
                                    </h3>

                                    <Link href="/project-details">
                                        <a className="work-btn">Read More</a>
                                    </Link>
                                </div>
                            </div>
                        </div>

                        <div className="col-lg-4 col-sm-6">
                            <div className="work-card">
                                <Image
                                    alt=''
                                    src='/work/work8.jpg'
                                    width={416}
                                    height={571}
                                />

                                <div className="content">
                                    <span>
                                        <Link href="/projects2#">
                                            <a>Bootstrap</a>
                                        </Link>
                                    </span>
                                    <h3>
                                        <Link href="/project-details">
                                            <a>Bootstrap Redesigning the New York times app</a>
                                        </Link>
                                    </h3>

                                    <Link href="/project-details">
                                        <a className="work-btn">Read More</a>
                                    </Link>
                                </div>
                            </div>
                        </div>

                        <div className="col-lg-4 col-sm-6">
                            <div className="work-card">
                                <Image
                                    alt=''
                                    src='/work/work9.jpg'
                                    width={416}
                                    height={571}
                                />

                                <div className="content">
                                    <span>
                                        <Link href="/projects2#">
                                            <a>App Development</a>
                                        </Link>
                                    </span>
                                    <h3>
                                        <Link href="/project-details">
                                            <a>We provide any type of app development</a>
                                        </Link>
                                    </h3>

                                    <Link href="/project-details">
                                        <a className="work-btn">Read More</a>
                                    </Link>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        );
    }
}

export default ProjectsThreeGrid;