import React, { Component, useEffect, useState } from 'react';
import Link from 'next/link';
import Image from 'next/image';
import { motion } from "framer-motion";
const slugify = require('slugify');

const Banner = props => {

    const [currentHeight, setCurrentHeight] = useState(1032);

    let { height } = props;

    useEffect(() => {
        setCurrentHeight(document.getElementById('main-title-area').clientHeight);
    }, []);

    console.log(currentHeight);

    return (
        <>
            <motion.div
                className={"main-banner page-title-area page-home-landing"}
                id="main-title-area"
                initial={{ height: currentHeight + 'px' }}
                animate={{ height: height }}
                transition={{ ease: "backInOut", delayChildren: 0.5 }}
            >

                <div className="container">
                    <div className="main-banner-content">
                        <span className="sub-title">Welcome to MXB Agency</span>
                        <h1>Creative &amp; Strategic Digital Marketing Agency</h1>

                        <div className="btn-box">
                            <Link href="/work">
                                <a className="default-btn">
                                    Our Work <span></span>
                                </a>
                            </Link>
                        </div>
                    </div>

                    <div className="main-banner-image">
                        <Image
                            alt=''
                            src='/home/banner-image1.png'
                            width={1524}
                            height={541}
                        />
                    </div>
                </div>

                {/* Animation shape Images */}
                <div className="shape-img2">
                    <Image
                        alt=''
                        src='/shapes/shape2.svg'
                        width={30}
                        height={30}
                    />
                </div>
                <div className="shape-img3">
                    <Image
                        alt=''
                        src='/shapes/shape3.svg'
                        width={25}
                        height={25}
                    />
                </div>

                <div className="shape-img4">
                    <Image
                        alt=''
                        src='/shapes/shape4.png'
                        width={26}
                        height={26}
                    />
                </div>
                <div className="shape-img5">
                    <Image
                        alt=''
                        src='/shapes/shape5.png'
                        width={21}
                        height={21}
                    />
                </div>
                <div className="shape-img7">
                    <Image
                        alt=''
                        src='/shapes/shape7.png'
                        width={80}
                        height={80}
                    />
                </div>
                <div className="shape-img10">
                    <Image
                        alt=''
                        src='/shapes/shape10.png'
                        width={22}
                        height={22}
                    />
                </div>
            </motion.div>
        </>
    );
}

export default Banner;