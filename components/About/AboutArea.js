import React, { Component } from 'react';
import Image from 'next/image';

class AboutArea extends Component {
    render() {
        return (
            <>
                <section className="about-area ptb-100">
                    <div className="container">
                        <div className="row align-items-center">
                            <div className="col-lg-6 col-md-12">
                                <div className="about-image">
                                    <Image
                                        alt='Creative + Digital'
                                        src='/about/about-img1.png'
                                        width={830}
                                        height={750}
                                    />
                                </div>
                            </div>

                            <div className="col-lg-6 col-md-12">
                                <div className="about-content">
                                    <span className="sub-title">Give Us The Jist</span>
                                    <h2>Creative + Digital</h2>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin fermentum dui commodo dapibus malesuada. Vestibulum massa nulla, vestibulum vel lorem nec, ullamcorper faucibus orci. Suspendisse a dapibus leo. Ut viverra tincidunt neque, at consequate.</p>

                                    <ul className="features-list">
                                        <li><span><i className="fas fa-check"></i> Leveraging Modern Tech</span></li>
                                        <li><span><i className="fas fa-check"></i> Efficient Processes</span></li>
                                        <li><span><i className="fas fa-check"></i> Omnichannel Approach</span></li>
                                        <li><span><i className="fas fa-check"></i> In-House Specialists</span></li>
                                        <li><span><i className="fas fa-check"></i> Dedicated Developers</span></li>
                                        <li><span><i className="fas fa-check"></i> A+ Customer Experience</span></li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div className="about-inner-area">
                            <div className="row">
                                <div className="col-lg-4 col-md-6 col-sm-6">
                                    <div className="about-text">
                                        <h3>Our History</h3>
                                        <p>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nullam vitae laoreet lectus. Donec vitae malesuada ex, et tempus mauris.</p>

                                        <ul className="features-list">
                                            <li><i className="flaticon-tick"></i> 20+ Years Experience</li>
                                            <li><i className="flaticon-tick"></i> Global Reach</li>
                                            <li><i className="flaticon-tick"></i> Rich Breadth of Industry</li>
                                        </ul>
                                    </div>
                                </div>

                                <div className="col-lg-4 col-md-6 col-sm-6">
                                    <div className="about-text">
                                        <h3>Our Mission</h3>
                                        <p>Nunc facilisis massa vitae nisl faucibus, non eleifend enim sollicitudin. Vestibulum posuere a nulla ut porta. Duis ullamcorper venenatis accumsan.</p>

                                        <ul className="features-list">
                                            <li><i className="flaticon-tick"></i> Never Settle. Always Improve</li>
                                            <li><i className="flaticon-tick"></i> Strategic Thinking</li>
                                            <li><i className="flaticon-tick"></i> Insight Driven Design</li>
                                        </ul>
                                    </div>
                                </div>

                                <div className="col-lg-4 col-md-6 col-sm-6 offset-lg-0 offset-md-3 offset-sm-3">
                                    <div className="about-text">
                                        <h3>Who We Are</h3>
                                        <p>Fusce placerat massa eget nunc pretium, non suscipit velit volutpat. In fringilla, libero sit amet egestas semper, nisi nunc ornare nisi, nec varius purus massa vel orci.</p>

                                        <ul className="features-list">
                                            <li><i className="flaticon-tick"></i> Human Approach</li>
                                            <li><i className="flaticon-tick"></i> Thinkers &amp; Doers</li>
                                            <li><i className="flaticon-tick"></i> Ambitious Overachievers</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    {/* Animation Shape Images */}
                    <div className="shape-img3">
                        <Image
                            alt=''
                            src='/shapes/shape3.svg'
                            width={25}
                            height={25}
                        />

                    </div>
                    <div className="shape-img2">
                        <Image
                            alt=''
                            src='/shapes/shape2.svg'
                            width={30}
                            height={30}
                        />
                    </div>
                </section>
            </>
        );
    }
}

export default AboutArea;