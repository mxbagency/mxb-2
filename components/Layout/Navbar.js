import React, { Component } from 'react';
import { connect } from 'react-redux';
import Link from '../../utils/ActiveLink';
import SidebarModal from '../SidebarModal/SidebarModal';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { faLightbulbOn } from '@fortawesome/pro-light-svg-icons';
import { faBullseyeArrow } from '@fortawesome/pro-light-svg-icons';
import { faPalette } from '@fortawesome/pro-light-svg-icons';
import { faShareAlt } from '@fortawesome/pro-light-svg-icons';
import { faLaptopCode } from '@fortawesome/pro-light-svg-icons';

class Navbar extends Component {

    // Sidebar Modal
    state = {
        sidebarModal: false
    };
    toggleModal = () => {
        this.setState({
            sidebarModal: !this.state.sidebarModal
        });
    }

    // Search Form
    state = {
        searchForm: false,
    };
    handleSearchForm = () => {
        this.setState(prevState => {
            return {
                searchForm: !prevState.searchForm
            };
        });
    }

    // Navbar
    _isMounted = false;
    state = {
        display: false,
        collapsed: true
    };
    toggleNavbar = () => {
        this.setState({
            collapsed: !this.state.collapsed,
        });
    }
    componentDidMount() {
        let elementId = document.getElementById("navbar");
        document.addEventListener("scroll", () => {
            if (window.scrollY > 170) {
                elementId.classList.add("is-sticky");
            } else {
                elementId.classList.remove("is-sticky");
            }
        });
        window.scrollTo(0, 0);
    }
    componentWillUnmount() {
        this._isMounted = false;
    }

    render() {
        let { products } = this.props;
        const { collapsed } = this.state;
        const classOne = collapsed ? 'collapse navbar-collapse' : 'collapse navbar-collapse show';
        const classTwo = collapsed ? 'navbar-toggler navbar-toggler-right collapsed' : 'navbar-toggler navbar-toggler-right';

        return (
            <>
                <div id="navbar" className="navbar-area">
                    <div className="adani-nav">
                        <div className="container">
                            <nav className="navbar navbar-expand-md navbar-light">
                                <Link href="/">
                                    <a className="navbar-brand">
                                        <div className="mxb-logo">
                                            <div>
                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 910.32 307.56" className="injected-svg" xmlnsXlink="http://www.w3.org/1999/xlink">
                                                    <title>MXB</title>
                                                    <g id="Layer_2" data-name="Layer 2">
                                                        <g id="Layer_1-2" data-name="Layer 1">
                                                            <path d="M815,246.05h-61.3A8.75,8.75,0,0,1,745,237.3V186.45a8.75,8.75,0,0,1,8.75-8.75H815c20.56,0,35.39,13.41,35.39,33.94,0,21-14.83,34.41-35.39,34.41M745,63.43a8.75,8.75,0,0,1,8.75-8.75h52.12c19.22,0,32.78,14.93,32.78,34.17S824.64,123,805.85,123H753.73a8.75,8.75,0,0,1-8.75-8.75Zm125.14,83.21C888,132.23,899,112,899,85.76,899,34.65,857.42,0,805.85,0H692.22a8.75,8.75,0,0,0-8.75,8.75V298.81a8.75,8.75,0,0,0,8.75,8.75H815c52.9,0,95.29-37.41,95.29-89.85,0-31.93-15.75-57.09-40.2-71.07" fill="#fff"></path>
                                                            <path d="M290.79,0H243.25c-3,0-5.86,2.3-7.45,4.89L157,134.44a8.72,8.72,0,0,1-14.89.18L63.11,4.8C61.52,2.2,58.7,0,55.66,0H7.8C3,0,0,5.33,0,10.15V300.53c0,4.83,3,7,7.8,7H50.89c4.82,0,8.72-3,8.73-7.86L60,137.25c0-8.89,11.77-11.85,16.26-4.18l65.82,112.59a8.73,8.73,0,0,0,15,.16l65.6-108.48c4.56-7.55,16.16-4.33,16.2,4.49l.55,157.88c0,4.81,3.92,7.85,8.73,7.85h42.66c4.82,0,9.94-2.2,9.94-7V10.15c0-4.82-5.12-10.15-9.94-10.15" fill="#fff"></path>
                                                            <path d="M528.32,155.66a9.21,9.21,0,0,1,0-9.45l77.9-131.54C609.89,8.52,605.45,0,598.29,0H551.37c-3.26,0-6.28,2.43-7.94,5.23L499.3,79.92a9.23,9.23,0,0,1-15.87.19l-44.29-75C437.48,2.34,434.46,0,431.21,0H383.39c-6.77,0-11,8.09-7.49,13.9l78.51,132.15a9.56,9.56,0,0,1,0,9.63L371.51,294.37c-3.66,6.16.77,13.19,7.94,13.19h46.93c3.26,0,6.27-.86,7.94-3.66l49.24-82.35a9.24,9.24,0,0,1,15.87.2l49.32,82.05c1.67,2.79,4.68,3.76,7.93,3.76h46.87c7.16,0,11.6-6.95,7.93-13.11Z" fill="#fff"></path>
                                                            <rect width="910.32" height="307.56" fill="none"></rect>
                                                        </g>
                                                    </g>
                                                </svg>
                                            </div>
                                        </div>
                                    </a>
                                </Link>

                                <button
                                    onClick={this.toggleNavbar}
                                    className={classTwo}
                                    type="button"
                                    data-toggle="collapse"
                                    data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                                    aria-expanded="false"
                                    aria-label="Toggle navigation"
                                >
                                    <span className="icon-bar top-bar"></span>
                                    <span className="icon-bar middle-bar"></span>
                                    <span className="icon-bar bottom-bar"></span>
                                </button>

                                <div className={classOne} id="navbarSupportedContent">
                                    <ul className="navbar-nav">

                                        <li className="nav-item">
                                            <Link href="/" activeClassName="active">
                                                <a className="nav-link">Home</a>
                                            </Link>
                                        </li>

                                        <li className="nav-item">
                                            <Link href="/about" activeClassName="active">
                                                <a className="nav-link">About Us</a>
                                            </Link>
                                        </li>

                                        <li className="nav-item">
                                            <Link href="/work" activeClassName="active">
                                                <a className="nav-link">Our Work</a>
                                            </Link>
                                        </li>



                                        <li className="nav-item has-dropdown">
                                            <Link href="#" activeClassName="active">
                                                <a className="nav-link" onClick={e => e.preventDefault()}>
                                                    Services <i className="fas fa-chevron-down"></i>
                                                </a>
                                            </Link>



                                            <ul className="dropdown-menu">

                                                <div className="inner">

                                                    <div className="menu-section brand">
                                                        <FontAwesomeIcon className="menu-icon" icon={faLightbulbOn} />
                                                        <h3>Brand</h3>
                                                        <ul className="inner-sub-menu">
                                                            <li className="nav-item">
                                                                <Link href="/services/brand-identity" activeClassName="active">
                                                                    <a className="nav-link">Brand Identity</a>
                                                                </Link>
                                                            </li>
                                                            <li className="nav-item">
                                                                <Link href="/services/brand-strategy" activeClassName="active">
                                                                    <a className="nav-link">Brand Strategy</a>
                                                                </Link>
                                                            </li>
                                                            <li className="nav-item">
                                                                <Link href="/services/brand-positioning" activeClassName="active">
                                                                    <a className="nav-link">Brand Positioning</a>
                                                                </Link>
                                                            </li>
                                                            <li className="nav-item">
                                                                <Link href="/services/brand-architecture" activeClassName="active">
                                                                    <a className="nav-link">Brand Architecture</a>
                                                                </Link>
                                                            </li>
                                                        </ul>

                                                        <ul className="inner-sub-menu extra-sub-menu">
                                                            <li className="nav-item">
                                                                <Link href="/contact" activeClassName="active">
                                                                    <a className="default-btn">Start Project</a>
                                                                </Link>
                                                            </li>
                                                            <li className="nav-item">
                                                                <Link href="/services" activeClassName="active">
                                                                    <a className="nav-link">Services Overview</a>
                                                                </Link>
                                                            </li>

                                                        </ul>
                                                    </div>

                                                    <div className="menu-section strategy">
                                                        <FontAwesomeIcon className="menu-icon" icon={faBullseyeArrow} />
                                                        <h3>Strategy</h3>
                                                        <ul className="inner-sub-menu">
                                                            <li className="nav-item">
                                                                <Link href="/services/research-consultation" activeClassName="active">
                                                                    <a className="nav-link">Research &amp; Consultation</a>
                                                                </Link>
                                                            </li>
                                                            <li className="nav-item">
                                                                <Link href="/services/data-analytics" activeClassName="active">
                                                                    <a className="nav-link">Data &amp; Analytics</a>
                                                                </Link>
                                                            </li>
                                                            <li className="nav-item">
                                                                <Link href="/services/competitor-analysis" activeClassName="active">
                                                                    <a className="nav-link">Competitor Analysis</a>
                                                                </Link>
                                                            </li>
                                                            <li className="nav-item">
                                                                <Link href="/services/audience-development" activeClassName="active">
                                                                    <a className="nav-link">Audience Development</a>
                                                                </Link>
                                                            </li>

                                                            <li className="nav-item">
                                                                <Link href="/services/public-relations" activeClassName="active">
                                                                    <a className="nav-link">Public Relations</a>
                                                                </Link>
                                                            </li>

                                                            <li className="nav-item">
                                                                <Link href="/services/shopper-marketing" activeClassName="active">
                                                                    <a className="nav-link">Shopper Marketing</a>
                                                                </Link>
                                                            </li>
                                                        </ul>
                                                    </div>

                                                    <div className="menu-section creative">
                                                        <FontAwesomeIcon className="menu-icon" icon={faPalette} />
                                                        <h3>Creative</h3>
                                                        <ul className="inner-sub-menu">
                                                            <li className="nav-item">
                                                                <Link href="/services/ideation" activeClassName="active">
                                                                    <a className="nav-link">Ideation</a>
                                                                </Link>
                                                            </li>
                                                            <li className="nav-item">
                                                                <Link href="/services/art-direction" activeClassName="active">
                                                                    <a className="nav-link">Art Direction</a>
                                                                </Link>
                                                            </li>
                                                            <li className="nav-item">
                                                                <Link href="/services/graphic-design" activeClassName="active">
                                                                    <a className="nav-link">Graphic Design</a>
                                                                </Link>
                                                            </li>
                                                            <li className="nav-item">
                                                                <Link href="/services/illustration" activeClassName="active">
                                                                    <a className="nav-link">Illustration</a>
                                                                </Link>
                                                            </li>

                                                            <li className="nav-item">
                                                                <Link href="/services/motion-graphics" activeClassName="active">
                                                                    <a className="nav-link">Motion Graphics</a>
                                                                </Link>
                                                            </li>

                                                            <li className="nav-item">
                                                                <Link href="/services/photography-videography" activeClassName="active">
                                                                    <a className="nav-link">Photography &amp; Videography</a>
                                                                </Link>
                                                            </li>
                                                            <li className="nav-item">
                                                                <Link href="/services/copywriting" activeClassName="active">
                                                                    <a className="nav-link">Copywriting</a>
                                                                </Link>
                                                            </li>
                                                        </ul>
                                                    </div>

                                                    <div className="menu-section digital">
                                                        <FontAwesomeIcon className="menu-icon" icon={faShareAlt} />
                                                        <h3>Digital</h3>
                                                        <ul className="inner-sub-menu">
                                                            <li className="nav-item">
                                                                <Link href="/services/social-media-marketing" activeClassName="active">
                                                                    <a className="nav-link">Social Media Marketing</a>
                                                                </Link>
                                                            </li>
                                                            <li className="nav-item">
                                                                <Link href="/services/social-media-management" activeClassName="active">
                                                                    <a className="nav-link">Social Media Management</a>
                                                                </Link>
                                                            </li>
                                                            <li className="nav-item">
                                                                <Link href="/services/email-marketing" activeClassName="active">
                                                                    <a className="nav-link">Email Marketing</a>
                                                                </Link>
                                                            </li>
                                                            <li className="nav-item">
                                                                <Link href="/services/pay-per-click" activeClassName="active">
                                                                    <a className="nav-link">Pay-Per-Click</a>
                                                                </Link>
                                                            </li>

                                                            <li className="nav-item">
                                                                <Link href="/services/display-advertising" activeClassName="active">
                                                                    <a className="nav-link">Display Advertising</a>
                                                                </Link>
                                                            </li>

                                                            <li className="nav-item">
                                                                <Link href="/services/influencer-marketing" activeClassName="active">
                                                                    <a className="nav-link">Influencer Marketing</a>
                                                                </Link>
                                                            </li>
                                                            <li className="nav-item">
                                                                <Link href="/services/seo" activeClassName="active">
                                                                    <a className="nav-link">SEO</a>
                                                                </Link>
                                                            </li>
                                                        </ul>
                                                    </div>

                                                    <div className="menu-section development">
                                                        <FontAwesomeIcon className="menu-icon" icon={faLaptopCode} />
                                                        <h3>Development</h3>
                                                        <ul className="inner-sub-menu">
                                                            <li className="nav-item">
                                                                <Link href="/services/ui-ux-design" activeClassName="active">
                                                                    <a className="nav-link">UI &amp; UX Design</a>
                                                                </Link>
                                                            </li>
                                                            <li className="nav-item">
                                                                <Link href="/services/web-development" activeClassName="active">
                                                                    <a className="nav-link">Web Development</a>
                                                                </Link>
                                                            </li>
                                                            <li className="nav-item">
                                                                <Link href="/services/cms-development" activeClassName="active">
                                                                    <a className="nav-link">CMS Development</a>
                                                                </Link>
                                                            </li>
                                                            <li className="nav-item">
                                                                <Link href="/services/e-commerce-development" activeClassName="active">
                                                                    <a className="nav-link">E-Commerce Development</a>
                                                                </Link>
                                                            </li>

                                                            <li className="nav-item">
                                                                <Link href="/services/hosting-domains" activeClassName="active">
                                                                    <a className="nav-link">Hosting &amp; Domains</a>
                                                                </Link>
                                                            </li>

                                                            <li className="nav-item">
                                                                <Link href="/services/support-packages" activeClassName="active">
                                                                    <a className="nav-link">Support Packages</a>
                                                                </Link>
                                                            </li>
                                                        </ul>
                                                    </div>

                                                </div>
                                            </ul>

                                        </li>

                                        <li className="nav-item">
                                            <Link href="/blog" activeClassName="active">
                                                <a className="nav-link">Blog</a>
                                            </Link>
                                        </li>

                                        <li className="nav-item">
                                            <Link href="/careers" activeClassName="active">
                                                <a className="nav-link">Careers<span>5</span></a>

                                            </Link>
                                        </li>


                                        <li className="nav-item">
                                            <Link href="/contact" activeClassName="active">
                                                <a className="nav-link">Contact</a>
                                            </Link>
                                        </li>





                                        {/* <li className="nav-item">
                                            <Link href="#">
                                                <a className="nav-link" onClick={e => e.preventDefault()}>
                                                    Pages <i className="fas fa-chevron-down"></i>
                                                </a>
                                            </Link>

                                            <ul className="dropdown-menu">
                                                <li className="nav-item">
                                                    <Link href="/about" activeClassName="active">
                                                        <a className="nav-link">About Us Style One</a>
                                                    </Link>
                                                </li>
                                                <li className="nav-item">
                                                    <Link href="/about2" activeClassName="active">
                                                        <a className="nav-link">About Us Style Two</a>
                                                    </Link>
                                                </li>
                                                <li className="nav-item">
                                                    <Link href="/team" activeClassName="active">
                                                        <a className="nav-link">Team Style One</a>
                                                    </Link>
                                                </li>
                                                <li className="nav-item">
                                                    <Link href="/team2" activeClassName="active">
                                                        <a className="nav-link">Team Style Two</a>
                                                    </Link>
                                                </li>
                                                <li className="nav-item">
                                                    <Link href="/features" activeClassName="active">
                                                        <a className="nav-link">Features</a>
                                                    </Link>
                                                </li>
                                                <li className="nav-item">
                                                    <Link href="/pricing" activeClassName="active">
                                                        <a className="nav-link">Pricing</a>
                                                    </Link>
                                                </li>
                                                <li className="nav-item">
                                                    <Link href="/partner" activeClassName="active">
                                                        <a className="nav-link">Partner</a>
                                                    </Link>
                                                </li>
                                                <li className="nav-item">
                                                    <Link href="/login" activeClassName="active">
                                                        <a className="nav-link">Login</a>
                                                    </Link>
                                                </li>
                                                <li className="nav-item">
                                                    <Link href="/signup" activeClassName="active">
                                                        <a className="nav-link">Signup</a>
                                                    </Link>
                                                </li>
                                                <li className="nav-item">
                                                    <Link href="/faq" activeClassName="active">
                                                        <a className="nav-link">FAQ</a>
                                                    </Link>
                                                </li>
                                                <li className="nav-item">
                                                    <Link href="/error" activeClassName="active">
                                                        <a className="nav-link">404 Error Page</a>
                                                    </Link>
                                                </li>
                                            </ul>
                                        </li> */}




                                    </ul>

                                    <div className="others-options">

                                        <div className="option-item">
                                            <i
                                                onClick={this.handleSearchForm}
                                                className="search-btn flaticon-search"
                                                style={{
                                                    display: this.state.searchForm ? 'none' : 'block'
                                                }}
                                            ></i>

                                            <i
                                                onClick={this.handleSearchForm}
                                                className={`close-btn flaticon-close ${this.state.searchForm ? 'active' : ''}`}
                                            ></i>

                                            <div
                                                className="search-overlay search-popup"
                                                style={{
                                                    display: this.state.searchForm ? 'block' : 'none'
                                                }}
                                            >
                                                <div className='search-box'>
                                                    <form className="search-form">
                                                        <input className="search-input" name="search" placeholder="Search" type="text" />
                                                        <button className="search-button" type="submit">
                                                            <i className="fas fa-search"></i>
                                                        </button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>

                {/* Right Sidebar Modal */}
                <SidebarModal onClick={this.toggleModal} active={this.state.sidebarModal ? 'active' : ''} />
            </>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        products: state.addedItems
    }
}

export default connect(mapStateToProps)(Navbar);

// export default Navbar;
