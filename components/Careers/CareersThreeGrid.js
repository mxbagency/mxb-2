import React, { Component } from 'react';
import Link from 'next/link';
import Image from 'next/image';

class CareersThreeGrid extends Component {
    render() {
        return (
            <div className="careers-area ptb-100">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-4 col-sm-6">
                            <div className="career-card">
                                <div className="content">
                                    <span>
                                        <Link href="/careers/1">
                                            <a>Permanent | Full Time</a>
                                        </Link>
                                    </span>
                                    <h3>
                                        <Link href="/careers/1">
                                            <a>Digital Media Executive</a>
                                        </Link>
                                    </h3>

                                    <Link href="/careers/1">
                                        <a className="career-btn">Read More</a>
                                    </Link>
                                </div>
                            </div>
                        </div>

                        <div className="col-lg-4 col-sm-6">
                            <div className="career-card">
                                <div className="content">
                                    <span>
                                        <Link href="/careers/1">
                                            <a>Permanent | Part Time</a>
                                        </Link>
                                    </span>
                                    <h3>
                                        <Link href="/careers/1">
                                            <a>Accounts Assistant</a>
                                        </Link>
                                    </h3>

                                    <Link href="/careers/1">
                                        <a className="career-btn">Read More</a>
                                    </Link>
                                </div>
                            </div>
                        </div>

                        <div className="col-lg-4 col-sm-6">
                            <div className="career-card">
                                <div className="content">
                                    <span>
                                        <Link href="/careers/1">
                                            <a>Permanent | Full Time</a>
                                        </Link>
                                    </span>
                                    <h3>
                                        <Link href="/careers/1">
                                            <a>UI/UX Designer</a>
                                        </Link>
                                    </h3>

                                    <Link href="/careers/1">
                                        <a className="career-btn">Read More</a>
                                    </Link>
                                </div>
                            </div>
                        </div>

                        <div className="col-lg-4 col-sm-6">
                            <div className="career-card">
                                <div className="content">
                                    <span>
                                        <Link href="/careers/1">
                                            <a>Permanent | Full Time</a>
                                        </Link>
                                    </span>
                                    <h3>
                                        <Link href="/careers/1">
                                            <a>Digital Content Creator</a>
                                        </Link>
                                    </h3>

                                    <Link href="/careers/1">
                                        <a className="career-btn">Read More</a>
                                    </Link>
                                </div>
                            </div>
                        </div>

                        <div className="col-lg-4 col-sm-6">
                            <div className="career-card">
                                <div className="content">
                                    <span>
                                        <Link href="/careers/1">
                                            <a>Permanent | Full Time</a>
                                        </Link>
                                    </span>
                                    <h3>
                                        <Link href="/careers/1">
                                            <a>Junior Account Manager</a>
                                        </Link>
                                    </h3>

                                    <Link href="/careers/1">
                                        <a className="career-btn">Read More</a>
                                    </Link>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        );
    }
}

export default CareersThreeGrid;