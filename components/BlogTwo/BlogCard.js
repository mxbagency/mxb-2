import React, { Component, useEffect, useState } from 'react';
import Link from 'next/link';
import Image from 'next/image';
import axios from 'axios';
import Moment from 'react-moment';
import TextTruncate from 'react-text-truncate';




const BlogCard = () => {

    const [blogs, setBlogs] = useState([]);

    useEffect(() => {

        axios.get('https://still-forest-77731.herokuapp.com/blogs').then(response => {
            const returnedElement = response.data;
            const sortedData = returnedElement.sort((a, b) => new Date(b.published_at) - new Date(a.published_at));
            setBlogs(sortedData);
        });

    }, []);

    return (
        <>

            <div className="row">
                {blogs.map((blog, i) => {
                    // console.log(blog);
                    const authorName = blog.admin_user.firstname + ' ' + blog.admin_user.lastname;
                    const featuredImage = blog.FeaturedImage.url;
                    return <>
                        <div className="col-lg-6 col-md-6">
                            <div className="single-blog-post">
                                <div className="post-image">
                                    <Link href="/blog-details">
                                        <a>
                                            <img
                                                alt={blog.Title}
                                                src={featuredImage}
                                                width={featuredImage.width}
                                                height={featuredImage.height}
                                            />
                                        </a>
                                    </Link>
                                </div>

                                <div className="post-content">
                                    <div className="post-meta">
                                        <ul>
                                            <li>
                                                By
                                                <Link href="/blog">
                                                    <a>{authorName}</a>
                                                </Link>
                                            </li>
                                            <li><Moment format="Do MMMM YYYY">
                                                {blog.published_at}
                                            </Moment></li>
                                        </ul>
                                    </div>
                                    <h3>
                                        <Link href="/blog-details">
                                            <a>{blog.Title}</a>
                                        </Link>
                                    </h3>
                                    <p>
                                        <TextTruncate
                                            line={3}
                                            truncateText="…"
                                            text={blog.Content}
                                        />
                                    </p>

                                    <Link href="/blog-details">
                                        <a className="read-more-btn">
                                            Read More <i className="flaticon-right-arrow"></i>
                                        </a>
                                    </Link>
                                </div>
                            </div>
                        </div>
                    </>


                })}




                {/* <div className="col-lg-6 col-md-6">
                    <div className="single-blog-post">
                        <div className="post-image">
                            <Link href="/blog-details">
                                <a>
                                    <Image
                                        alt=''
                                        src='/blog/blog-image1.jpg'
                                        width={860}
                                        height={700}
                                    />
                                </a>
                            </Link>
                        </div>
                        <div className="post-content">
                            <div className="post-meta">
                                <ul>
                                    <li>
                                        By
                                        <Link href="/blog">
                                            <a>Sarah Taylor</a>
                                        </Link>
                                    </li>
                                    <li>June 24, 2019</li>
                                </ul>
                            </div>
                            <h3>
                                <Link href="/blog-details">
                                    <a>How To Boost Your Digital Marketing Agency</a>
                                </Link>
                            </h3>
                            <p>Lorem ipsum dolor sit amet, constetur adipiscing elit, sed do eiusmod tempor incididunt.</p>

                            <Link href="/blog-details">
                                <a className="read-more-btn">
                                    Read More <i className="flaticon-right-arrow"></i>
                                </a>
                            </Link>
                        </div>
                    </div>
                </div>

                <div className="col-lg-6 col-md-6">
                    <div className="single-blog-post">
                        <div className="post-image">
                            <Link href="/blog-details">
                                <a>
                                    <Image
                                        alt=''
                                        src='/blog/blog-image2.jpg'
                                        width={860}
                                        height={700}
                                    />
                                </a>
                            </Link>
                        </div>
                        <div className="post-content">
                            <div className="post-meta">
                                <ul>
                                    <li>By
                                        <Link href="/blog">
                                            <a>James Anderson</a>
                                        </Link>
                                    </li>
                                    <li>June 26, 2019</li>
                                </ul>
                            </div>
                            <h3>
                                <Link href="/blog-details">
                                    <a>The Rise Of Smarketing And Why You Need It</a>
                                </Link>
                            </h3>
                            <p>Lorem ipsum dolor sit amet, constetur adipiscing elit, sed do eiusmod tempor incididunt.</p>

                            <Link href="/blog-details">
                                <a className="read-more-btn">
                                    Read More <i className="flaticon-right-arrow"></i>
                                </a>
                            </Link>
                        </div>
                    </div>
                </div>

                <div className="col-lg-6 col-md-6">
                    <div className="single-blog-post">
                        <div className="post-image">
                            <Link href="/blog-details">
                                <a>
                                    <Image
                                        alt=''
                                        src='/blog/blog-image3.jpg'
                                        width={860}
                                        height={700}
                                    />
                                </a>
                            </Link>
                        </div>
                        <div className="post-content">
                            <div className="post-meta">
                                <ul>
                                    <li>
                                        By
                                        <Link href="/blog">
                                            <a>Steven Smith</a>
                                        </Link>
                                    </li>
                                    <li>June 25, 2019</li>
                                </ul>
                            </div>
                            <h3>
                                <Link href="/blog-details">
                                    <a>How To Use Music To Boost Your Business</a>
                                </Link>
                            </h3>
                            <p>Lorem ipsum dolor sit amet, constetur adipiscing elit, sed do eiusmod tempor incididunt.</p>

                            <Link href="/blog-details">
                                <a className="read-more-btn">
                                    Read More <i className="flaticon-right-arrow"></i>
                                </a>
                            </Link>
                        </div>
                    </div>
                </div>

                <div className="col-lg-6 col-md-6">
                    <div className="single-blog-post">
                        <div className="post-image">
                            <Link href="/blog-details">
                                <a>
                                    <Image
                                        alt=''
                                        src='/blog/blog-image4.jpg'
                                        width={860}
                                        height={700}
                                    />
                                </a>
                            </Link>
                        </div>
                        <div className="post-content">
                            <div className="post-meta">
                                <ul>
                                    <li>
                                        By
                                        <Link href="/blog">
                                            <a>Sarah Taylor</a>
                                        </Link>
                                    </li>
                                    <li>June 24, 2019</li>
                                </ul>
                            </div>
                            <h3>
                                <Link href="/blog-details">
                                    <a>Creative solutions to improve your business!</a>
                                </Link>
                            </h3>
                            <p>Lorem ipsum dolor sit amet, constetur adipiscing elit, sed do eiusmod tempor incididunt.</p>

                            <Link href="/blog-details">
                                <a className="read-more-btn">
                                    Read More <i className="flaticon-right-arrow"></i>
                                </a>
                            </Link>
                        </div>
                    </div>
                </div>

                <div className="col-lg-6 col-md-6">
                    <div className="single-blog-post">
                        <div className="post-image">
                            <Link href="/blog-details">
                                <a>
                                    <Image
                                        alt=''
                                        src='/blog/blog-image5.jpg'
                                        width={860}
                                        height={700}
                                    />
                                </a>
                            </Link>
                        </div>
                        <div className="post-content">
                            <div className="post-meta">
                                <ul>
                                    <li>
                                        By
                                        <Link href="/blog">
                                            <a>James Anderson</a>
                                        </Link>
                                    </li>
                                    <li>June 26, 2019</li>
                                </ul>
                            </div>
                            <h3>
                                <Link href="/blog-details">
                                    <a>Finding the human in technology</a>
                                </Link>
                            </h3>
                            <p>Lorem ipsum dolor sit amet, constetur adipiscing elit, sed do eiusmod tempor incididunt.</p>

                            <Link href="/blog-details">
                                <a className="read-more-btn">
                                    Read More <i className="flaticon-right-arrow"></i>
                                </a>
                            </Link>
                        </div>
                    </div>
                </div>

                <div className="col-lg-6 col-md-6">
                    <div className="single-blog-post">
                        <div className="post-image">
                            <Link href="/blog-details">
                                <a>
                                    <Image
                                        alt=''
                                        src='/blog/blog-image6.jpg'
                                        width={860}
                                        height={700}
                                    />
                                </a>
                            </Link>
                        </div>
                        <div className="post-content">
                            <div className="post-meta">
                                <ul>
                                    <li>
                                        By
                                        <Link href="/blog">
                                            <a>Steven Smith</a>
                                        </Link>
                                    </li>
                                    <li>June 25, 2019</li>
                                </ul>
                            </div>
                            <h3>
                                <Link href="/blog-details">
                                    <a>Ideas people want to spend time with</a>
                                </Link>
                            </h3>
                            <p>Lorem ipsum dolor sit amet, constetur adipiscing elit, sed do eiusmod tempor incididunt.</p>

                            <Link href="/blog-details">
                                <a className="read-more-btn">
                                    Read More <i className="flaticon-right-arrow"></i>
                                </a>
                            </Link>
                        </div>
                    </div>
                </div>

                <div className="col-lg-6 col-md-6">
                    <div className="single-blog-post">
                        <div className="post-image">
                            <Link href="/blog-details">
                                <a>
                                    <Image
                                        alt=''
                                        src='/blog/blog-image7.jpg'
                                        width={860}
                                        height={700}
                                    />
                                </a>
                            </Link>
                        </div>
                        <div className="post-content">
                            <div className="post-meta">
                                <ul>
                                    <li>
                                        By
                                        <Link href="/blog">
                                            <a>Sarah Taylor</a>
                                        </Link>
                                    </li>
                                    <li>June 24, 2019</li>
                                </ul>
                            </div>
                            <h3>
                                <Link href="/blog-details">
                                    <a>Ideas people want to spend time with</a>
                                </Link>
                            </h3>
                            <p>Lorem ipsum dolor sit amet, constetur adipiscing elit, sed do eiusmod tempor incididunt.</p>

                            <Link href="/blog-details">
                                <a className="read-more-btn">
                                    Read More <i className="flaticon-right-arrow"></i>
                                </a>
                            </Link>
                        </div>
                    </div>
                </div>

                <div className="col-lg-6 col-md-6">
                    <div className="single-blog-post">
                        <div className="post-image">
                            <Link href="/blog-details">
                                <a>
                                    <Image
                                        alt=''
                                        src='/blog/blog-image8.jpg'
                                        width={860}
                                        height={700}
                                    />
                                </a>
                            </Link>
                        </div>
                        <div className="post-content">
                            <div className="post-meta">
                                <ul>
                                    <li>
                                        By
                                        <Link href="/blog">
                                            <a>James Anderson</a>
                                        </Link>
                                    </li>
                                    <li>June 26, 2019</li>
                                </ul>
                            </div>
                            <h3>
                                <Link href="/blog-details">
                                    <a>Ideas people want to spend time with</a>
                                </Link>
                            </h3>
                            <p>Lorem ipsum dolor sit amet, constetur adipiscing elit, sed do eiusmod tempor incididunt.</p>

                            <Link href="/blog-details">
                                <a className="read-more-btn">
                                    Read More <i className="flaticon-right-arrow"></i>
                                </a>
                            </Link>
                        </div>
                    </div>
                </div> */}

                {/* Pagination  */}
                {/* <div className="col-lg-12 col-md-12">
                        <div className="pagination-area">
                            <Link href="#">
                                <a className="prev page-numbers">
                                    <i className="fas fa-angle-double-left"></i>
                                </a>
                            </Link>
                            <Link href="#">
                                <a className="page-numbers">1</a>
                            </Link>
                            <span className="page-numbers current" aria-current="page">2</span>
                            <Link href="#">
                                <a className="page-numbers">3</a>
                            </Link>
                            <Link href="#">
                                <a className="page-numbers">4</a>
                            </Link>
                            <Link href="#">
                                <a className="next page-numbers">
                                    <i className="fas fa-angle-double-right"></i>
                                </a>
                            </Link>
                        </div>
                    </div> */}
                {/* End Pagination  */}
            </div>
        </>
    );
}

export default BlogCard;