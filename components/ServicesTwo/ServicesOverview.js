import React, { Component } from 'react';
import Link from 'next/link';

class ServicesOverview extends Component {
    render() {
        return (
            <section className="overview-area ptb-100">
                <div className="container">
                    <div className="overview-box it-overview">
                        <div className="overview-content">
                            <div className="content">
                                <h2>Brand</h2>
                                <p>We believe brand interaction is key to communication. Real innovations and positive customer experience are the heart of success.</p>

                                <ul className="features-list">
                                    <li>
                                        <span>
                                            <i className='bx bxs-badge-check'></i>
                                            SEO Marketing
                                        </span>
                                    </li>
                                    <li>
                                        <span>
                                            <i className='bx bxs-badge-check'></i>
                                            Email Marketing
                                        </span>
                                    </li>
                                    <li>
                                        <span>
                                            <i className='bx bxs-badge-check'></i>
                                            Facebook Marketing
                                        </span>
                                    </li>
                                    <li>
                                        <span>
                                            <i className='bx bxs-badge-check'></i>
                                            Data Scraping
                                        </span>
                                    </li>
                                    <li>
                                        <span>
                                            <i className='bx bxs-badge-check'></i>
                                            Social Marketing
                                        </span>
                                    </li>
                                    <li>
                                        <span>
                                            <i className='bx bxs-badge-check'></i>
                                            Youtube Marketing
                                        </span>
                                    </li>
                                </ul>

                            </div>
                        </div>

                        <div className="overview-image">
                            <div className="image">
                                <img src="/images/services/it-service1.png" alt="image" />
                            </div>
                        </div>
                    </div>

                    <div className="overview-box it-overview">
                        <div className="overview-image">
                            <div className="image">
                                <img src="/images/services/it-service2.png" alt="image" />
                            </div>
                        </div>

                        <div className="overview-content">
                            <div className="content right-content">
                                <h2>Strategy</h2>
                                <p>We believe brand interaction is key to communication. Real innovations and positive customer experience are the heart of success.</p>

                                <ul className="features-list">
                                    <li>
                                        <span>
                                            <i className='bx bxs-badge-check'></i>
                                            Responsive Design
                                        </span>
                                    </li>
                                    <li>
                                        <span>
                                            <i className='bx bxs-badge-check'></i>
                                            UI / UX Design
                                        </span>
                                    </li>
                                    <li>
                                        <span>
                                            <i className='bx bxs-badge-check'></i>
                                            Mobile App Development
                                        </span>
                                    </li>
                                    <li>
                                        <span>
                                            <i className='bx bxs-badge-check'></i>
                                            Laravel Development
                                        </span>
                                    </li>
                                    <li>
                                        <span>
                                            <i className='bx bxs-badge-check'></i>
                                            React Development
                                        </span>
                                    </li>
                                    <li>
                                        <span>
                                            <i className='bx bxs-badge-check'></i>
                                            Angular Development
                                        </span>
                                    </li>
                                </ul>

                            </div>
                        </div>
                    </div>

                    <div className="overview-box it-overview">
                        <div className="overview-content">
                            <div className="content">
                                <h2>Creative</h2>
                                <p>We believe brand interaction is key to communication. Real innovations and positive customer experience are the heart of success.</p>

                                <ul className="features-list">
                                    <li>
                                        <span>
                                            <i className='bx bxs-badge-check'></i>
                                            Cloud Database
                                        </span>
                                    </li>
                                    <li>
                                        <span>
                                            <i className='bx bxs-badge-check'></i>
                                            Hybrid Cloud
                                        </span>
                                    </li>
                                    <li>
                                        <span>
                                            <i className='bx bxs-badge-check'></i>
                                            Email Servers
                                        </span>
                                    </li>
                                    <li>
                                        <span>
                                            <i className='bx bxs-badge-check'></i>
                                            Website Hosting
                                        </span>
                                    </li>
                                    <li>
                                        <span>
                                            <i className='bx bxs-badge-check'></i>
                                            File Storage
                                        </span>
                                    </li>
                                    <li>
                                        <span>
                                            <i className='bx bxs-badge-check'></i>
                                            Backup Systems
                                        </span>
                                    </li>
                                </ul>

                            </div>
                        </div>

                        <div className="overview-image">
                            <div className="image">
                                <img src="/images/services/it-service3.png" alt="image" />
                            </div>
                        </div>
                    </div>

                    <div className="overview-box it-overview">
                        <div className="overview-image">
                            <div className="image">
                                <img src="/images/services/it-service2.png" alt="image" />
                            </div>
                        </div>

                        <div className="overview-content">
                            <div className="content right-content">
                                <h2>Digital</h2>
                                <p>We believe brand interaction is key to communication. Real innovations and positive customer experience are the heart of success.</p>

                                <ul className="features-list">
                                    <li>
                                        <span>
                                            <i className='bx bxs-badge-check'></i>
                                            Responsive Design
                                        </span>
                                    </li>
                                    <li>
                                        <span>
                                            <i className='bx bxs-badge-check'></i>
                                            UI / UX Design
                                        </span>
                                    </li>
                                    <li>
                                        <span>
                                            <i className='bx bxs-badge-check'></i>
                                            Mobile App Development
                                        </span>
                                    </li>
                                    <li>
                                        <span>
                                            <i className='bx bxs-badge-check'></i>
                                            Laravel Development
                                        </span>
                                    </li>
                                    <li>
                                        <span>
                                            <i className='bx bxs-badge-check'></i>
                                            React Development
                                        </span>
                                    </li>
                                    <li>
                                        <span>
                                            <i className='bx bxs-badge-check'></i>
                                            Angular Development
                                        </span>
                                    </li>
                                </ul>

                            </div>
                        </div>
                    </div>

                    <div className="overview-box it-overview">
                        <div className="overview-content">
                            <div className="content">
                                <h2>Development</h2>
                                <p>We believe brand interaction is key to communication. Real innovations and positive customer experience are the heart of success.</p>

                                <ul className="features-list">
                                    <li>
                                        <span>
                                            <i className='bx bxs-badge-check'></i>
                                            UI &amp; UX Design
                                        </span>
                                    </li>
                                    <li>
                                        <span>
                                            <i className='bx bxs-badge-check'></i>
                                            Web Development
                                        </span>
                                    </li>
                                    <li>
                                        <span>
                                            <i className='bx bxs-badge-check'></i>
                                            CMS Development
                                        </span>
                                    </li>
                                    <li>
                                        <span>
                                            <i className='bx bxs-badge-check'></i>
                                            E-Commerce Development
                                        </span>
                                    </li>
                                    <li>
                                        <span>
                                            <i className='bx bxs-badge-check'></i>
                                            Hosting &amp; Domains
                                        </span>
                                    </li>
                                    <li>
                                        <span>
                                            <i className='bx bxs-badge-check'></i>
                                            Support Packages
                                        </span>
                                    </li>
                                </ul>

                            </div>
                        </div>

                        <div className="overview-image">
                            <div className="image">
                                <img src="/images/services/it-service3.png" alt="image" />
                            </div>
                        </div>
                    </div>

                </div>
            </section>
        );
    }
}

export default ServicesOverview;