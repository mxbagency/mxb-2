import React, { Component } from 'react';
import Link from 'next/link';
import dynamic from 'next/dynamic';
const ModalVideo = dynamic(() => import('react-modal-video'), {
    ssr: false
});

class Solution extends Component {

    state = {
        isOpen: false
    };

    openModal = () => {
        this.setState({ isOpen: true })
    };

    render() {
        return (
            <>
                <section className="solution-area ptb-100 jarallax">
                    <div className="container">
                        <div className="row align-items-center">
                            <div className="col-lg-6 col-md-12">
                                <div className="solution-content">
                                    <span className="sub-title">We Do Different</span>
                                    <h2>A Fusion of Specialisms</h2>
                                    <p>Nullam ut nibh eu leo posuere mattis non ac libero. Sed placerat viverra quam vel hendrerit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</p>

                                    <Link href="/contact">
                                        <a className="default-btn">Get In Touch <span></span></a>
                                    </Link>
                                </div>
                            </div>

                            <div className="col-lg-6 col-md-12">
                                <div className="solution-video">
                                    <div
                                        onClick={e => { e.preventDefault(); this.openModal() }}
                                        className="video-btn popup-youtube"
                                    >
                                        <i className="flaticon-play-button"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                {/* If you want change the video need to update below videoID */}
                <ModalVideo
                    channel='vimeo'
                    isOpen={this.state.isOpen}
                    byline={false}
                    title={false}
                    videoId='502874684'
                    color='6e4aff'
                    onClose={() => this.setState({ isOpen: false })}
                />
            </>
        );
    }
}

export default Solution;