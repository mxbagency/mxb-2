import React, { Component, useEffect, useState } from 'react';
import Link from 'next/link';
import Image from 'next/image';
import Head from 'next/head';
import { motion } from "framer-motion";
const slugify = require('slugify');


const PageHeader = props => {


    const [currentHeight, setCurrentHeight] = useState(document.getElementById('main-title-area').clientHeight);

    let { pageTitle, breadcrumbTextOne, breadcrumbTextTwo, breadcrumbUrl, height } = props;
    const pageSlug = slugify(pageTitle, { lower: true });

    useEffect(() => {
        setCurrentHeight(document.getElementById('main-title-area').clientHeight);
    }, []);

    // console.log(currentHeight);

    return (<>

        <Head>
            <title>{pageTitle} | MXB Agency - Digital Marketing Belfast</title>
            <meta property="og:title" content={pageTitle + ' | MXB Agency - Digital Marketing Belfast'} key="title" />
        </Head>


        <motion.div
            className={"page-title-area page-title-bg2 page-" + pageSlug}
            id="main-title-area"
            initial={{ height: currentHeight + 'px' }}
            animate={{ height: height }}

            transition={{ duration: 0.5, ease: [0.85, 0, 0.15, 1], delay: 0.25 }}
        >
            <div className="container">
                <div className="page-title-content">
                    <h2>{pageTitle}</h2>
                    <ul>
                        <li>
                            <Link href={breadcrumbUrl}>
                                <a>{breadcrumbTextOne}</a>
                            </Link>
                        </li>
                        <li>{breadcrumbTextTwo}</li>
                    </ul>
                </div>
            </div>

            {/* Animation Shape Image */}
            <div className="shape-img2">
                <Image
                    alt=''
                    src='/shapes/shape2.svg'
                    width={30}
                    height={30}
                />
            </div>
            <div className="shape-img3">
                <Image
                    alt=''
                    src='/shapes/shape3.svg'
                    width={25}
                    height={25}
                />
            </div>
            <div className="shape-img4">
                <Image
                    alt=''
                    src='/shapes/shape4.png'
                    width={26}
                    height={26}
                />
            </div>
            <div className="shape-img5">
                <Image
                    alt=''
                    src='/shapes/shape5.png'
                    width={21}
                    height={21}
                />
            </div>
            <div className="shape-img7">
                <Image
                    alt=''
                    src='/shapes/shape7.png'
                    width={80}
                    height={80}
                />
            </div>
            <div className="shape-img10">
                <Image
                    alt=''
                    src='/shapes/shape10.png'
                    width={22}
                    height={22}
                />
            </div>
        </motion.div>
    </>
    );

}

export default PageHeader;