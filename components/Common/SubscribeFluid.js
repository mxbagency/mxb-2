import React, { Component } from 'react';

class SubscribeFluid extends Component {
    render() {
        return (
            <>
                <section className="subscribe-area bg-F4F7FC">
                    <div className="container-fluid p-0">
                        <div className="subscribe-inner-area jarallax" data-jarallax='{"speed": 0.3}'>
                            <div className="subscribe-content">
                                <span className="sub-title">Join Our Newsletter</span>
                                <h2>Get the latest updates and tips from MXB straight to your inbox!</h2>

                                <form className="newsletter-form">
                                    <input type="email" className="input-newsletter" placeholder="Enter your email" name="email" />
                                    <button type="submit">Subscribe</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </section>
            </>
        );
    }
}

export default SubscribeFluid;