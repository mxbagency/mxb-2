import React, { Component } from 'react';

class FunFactsTwo extends Component {
    render() {
        return (
            <section className="fun-facts-two pt-100 pb-70 bg-f2f2f7">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-3 col-sm-6">
                            <div className="fun-fact-card">
                                <i className='bx bx-list-check'></i>
                                <h3>
                                    <span className="odometer">2326</span>
                                    <span className="sign-icon">+</span>
                                </h3>
                                <p>Successful Projects</p>
                            </div>
                        </div>

                        <div className="col-lg-3 col-sm-6">
                            <div className="fun-fact-card">
                                <i className='bx bx-smile'></i>
                                <h3>
                                    <span className="odometer">256</span>
                                    <span className="sign-icon">+</span>
                                </h3>
                                <p>Happy Clients</p>
                            </div>
                        </div>

                        <div className="col-lg-3 col-sm-6">
                            <div className="fun-fact-card">
                                <i className='bx bx-user-plus'></i>
                                <h3>
                                    <span className="odometer">32</span>
                                </h3>
                                <p>MXB'ers</p>
                            </div>
                        </div>

                        <div className="col-lg-3 col-sm-6">
                            <div className="fun-fact-card">
                                <i className='bx bx-trophy'></i>
                                <h3>
                                    <span className="odometer">29</span>
                                    <span className="sign-icon">+</span>
                                </h3>
                                <p>Awards</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

export default FunFactsTwo;