import React, { Component } from 'react';
import Link from 'next/link';
import Image from 'next/image';


const API_URL = process.env.WORDPRESS_API_URL;

async function fetchAPI(query, { variables } = {}) {
    // Set up some headers to tell the fetch call
    // that this is an application/json type
    const headers = { 'Content-Type': 'application/json' };

    // build out the fetch() call using the API_URL
    // environment variable pulled in at the start
    // Note the merging of the query and variables
    const res = await fetch(API_URL, {
        method: 'POST',
        headers,
        body: JSON.stringify({ query, variables })
    });

    // error handling work
    const json = await res.json();
    if (json.errors) {
        console.log(json.errors);
        console.log('error details', query, variables);
        throw new Error('Failed to fetch API');
    }
    return json.data;
}




class OurTeam extends Component {
    render() {
        return (
            <>
                <section className="team-area ptb-100 pb-70">
                    <div className="container">
                        <div className="section-title">
                            <span className="sub-title">Team MXB</span>
                            <h2>Meet Our Experts</h2>
                        </div>

                        <div className="row">
                            <div className="col-lg-3 col-md-6 col-sm-6">
                                <div className="single-team-box">
                                    <div className="image">
                                        <Image
                                            alt='Karen Carmichael'
                                            src='/team/Karen.jpg'
                                            width={600}
                                            height={540}
                                        />
                                    </div>

                                    <div className="content">
                                        <h3>Karen Carmichael</h3>
                                        <span>Managing Director</span>
                                    </div>
                                </div>
                            </div>

                            <div className="col-lg-3 col-md-6 col-sm-6">
                                <div className="single-team-box">
                                    <div className="image">
                                        <Image
                                            alt='David Mackey'
                                            src='/team/David-Mackey.jpg'
                                            width={600}
                                            height={540}
                                        />
                                    </div>

                                    <div className="content">
                                        <h3>David Mackey</h3>
                                        <span>Financial Director</span>
                                    </div>
                                </div>
                            </div>

                            <div className="col-lg-3 col-md-6 col-sm-6 offset-lg-0 offset-md-3 offset-sm-3">
                                <div className="single-team-box">
                                    <div className="image">
                                        <Image
                                            alt='Barry McCabe'
                                            src='/team/Barry-McCabe.jpg'
                                            width={600}
                                            height={540}
                                        />
                                    </div>

                                    <div className="content">
                                        <h3>Barry McCabe</h3>
                                        <span>Creative Director</span>
                                    </div>
                                </div>
                            </div>

                            <div className="col-lg-3 col-md-6 col-sm-6 offset-lg-0 offset-md-3 offset-sm-3">
                                <div className="single-team-box">
                                    <div className="image">
                                        <Image
                                            alt='Allie McAuley'
                                            src='/team/Allie.jpg'
                                            width={600}
                                            height={540}
                                        />
                                    </div>

                                    <div className="content">
                                        <h3>Allie McAuley</h3>
                                        <span>Client Services Director</span>
                                    </div>
                                </div>
                            </div>

                            <div className="col-lg-3 col-md-6 col-sm-6 offset-lg-0 offset-md-3 offset-sm-3">
                                <div className="single-team-box">
                                    <div className="image">
                                        <Image
                                            alt='Johanna McGuigan'
                                            src='/team/Johanna.jpg'
                                            width={600}
                                            height={540}
                                        />
                                    </div>

                                    <div className="content">
                                        <h3>Johanna McGuigan</h3>
                                        <span>Senior Account Director</span>
                                    </div>
                                </div>
                            </div>

                            <div className="col-lg-3 col-md-6 col-sm-6 offset-lg-0 offset-md-3 offset-sm-3">
                                <div className="single-team-box">
                                    <div className="image">
                                        <Image
                                            alt='Jo McAndrew'
                                            src='/team/Jo.jpg'
                                            width={600}
                                            height={540}
                                        />
                                    </div>

                                    <div className="content">
                                        <h3>Jo McAndrew</h3>
                                        <span>Operations &amp; Quality Manager</span>
                                    </div>
                                </div>
                            </div>

                            <div className="col-lg-3 col-md-6 col-sm-6 offset-lg-0 offset-md-3 offset-sm-3">
                                <div className="single-team-box">
                                    <div className="image">
                                        <Image
                                            alt='Kristam Moffett'
                                            src='/team/Kristam.jpg'
                                            width={600}
                                            height={540}
                                        />
                                    </div>

                                    <div className="content">
                                        <h3>Kristam Moffett</h3>
                                        <span>Head of Digital</span>
                                    </div>
                                </div>
                            </div>

                            <div className="col-lg-3 col-md-6 col-sm-6 offset-lg-0 offset-md-3 offset-sm-3">
                                <div className="single-team-box">
                                    <div className="image">
                                        <Image
                                            alt='Shay Reynolds'
                                            src='/team/Shay.jpg'
                                            width={600}
                                            height={540}
                                        />
                                    </div>

                                    <div className="content">
                                        <h3>Shay Reynolds</h3>
                                        <span>Senior Digital Creative</span>
                                    </div>
                                </div>
                            </div>

                            <div className="col-lg-3 col-md-6 col-sm-6 offset-lg-0 offset-md-3 offset-sm-3">
                                <div className="single-team-box">
                                    <div className="image">
                                        <Image
                                            alt=''
                                            src='/team/Chris.jpg'
                                            width={600}
                                            height={540}
                                        />
                                    </div>

                                    <div className="content">
                                        <h3>Chris Sloan</h3>
                                        <span>Senior Account Manager</span>
                                    </div>
                                </div>
                            </div>

                            <div className="col-lg-3 col-md-6 col-sm-6 offset-lg-0 offset-md-3 offset-sm-3">
                                <div className="single-team-box">
                                    <div className="image">
                                        <Image
                                            alt=''
                                            src='/team/Simon.jpg'
                                            width={600}
                                            height={540}
                                        />
                                    </div>

                                    <div className="content">
                                        <h3>Simon Warnock</h3>
                                        <span>Designer</span>
                                    </div>
                                </div>
                            </div>

                            <div className="col-lg-3 col-md-6 col-sm-6 offset-lg-0 offset-md-3 offset-sm-3">
                                <div className="single-team-box">
                                    <div className="image">
                                        <Image
                                            alt=''
                                            src='/team/David.jpg'
                                            width={600}
                                            height={540}
                                        />
                                    </div>

                                    <div className="content">
                                        <h3>David Moen</h3>
                                        <span>Senior Developer</span>
                                    </div>
                                </div>
                            </div>

                            <div className="col-lg-3 col-md-6 col-sm-6 offset-lg-0 offset-md-3 offset-sm-3">
                                <div className="single-team-box">
                                    <div className="image">
                                        <Image
                                            alt=''
                                            src='/team/Naoise.jpg'
                                            width={600}
                                            height={540}
                                        />
                                    </div>

                                    <div className="content">
                                        <h3>Naoise Muldoon</h3>
                                        <span>Digital Media Manager</span>
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-3 col-md-6 col-sm-6 offset-lg-0 offset-md-3 offset-sm-3">
                                <div className="single-team-box">
                                    <div className="image">
                                        <Image
                                            alt=''
                                            src='/team/HannahF.jpg'
                                            width={600}
                                            height={540}
                                        />
                                    </div>

                                    <div className="content">
                                        <h3>Hannah Faulkner</h3>
                                        <span>Creative</span>
                                    </div>
                                </div>
                            </div>

                            <div className="col-lg-3 col-md-6 col-sm-6 offset-lg-0 offset-md-3 offset-sm-3">
                                <div className="single-team-box">
                                    <div className="image">
                                        <Image
                                            alt=''
                                            src='/team/Katie.jpg'
                                            width={600}
                                            height={540}
                                        />
                                    </div>

                                    <div className="content">
                                        <h3>Katie Smith</h3>
                                        <span>Account Manager</span>
                                    </div>
                                </div>
                            </div>



                            <div className="col-lg-3 col-md-6 col-sm-6 offset-lg-0 offset-md-3 offset-sm-3">
                                <div className="single-team-box">
                                    <div className="image">
                                        <Image
                                            alt=''
                                            src='/team/Gwen.jpg'
                                            width={600}
                                            height={540}
                                        />
                                    </div>

                                    <div className="content">
                                        <h3>Gwendolyn Stone</h3>
                                        <span>Accounts Assistant</span>
                                    </div>
                                </div>
                            </div>

                            <div className="col-lg-3 col-md-6 col-sm-6 offset-lg-0 offset-md-3 offset-sm-3">
                                <div className="single-team-box">
                                    <div className="image">
                                        <Image
                                            alt=''
                                            src='/team/Aoife.jpg'
                                            width={600}
                                            height={540}
                                        />
                                    </div>

                                    <div className="content">
                                        <h3>Aoife Carney</h3>
                                        <span>Social Content Manager</span>
                                    </div>
                                </div>
                            </div>

                            <div className="col-lg-3 col-md-6 col-sm-6 offset-lg-0 offset-md-3 offset-sm-3">
                                <div className="single-team-box">
                                    <div className="image">
                                        <Image
                                            alt=''
                                            src='/team/David-Lee.jpg'
                                            width={600}
                                            height={540}
                                        />
                                    </div>

                                    <div className="content">
                                        <h3>David-Lee Badger</h3>
                                        <span>Designer</span>
                                    </div>
                                </div>
                            </div>

                            <div className="col-lg-3 col-md-6 col-sm-6 offset-lg-0 offset-md-3 offset-sm-3">
                                <div className="single-team-box">
                                    <div className="image">
                                        <Image
                                            alt=''
                                            src='/team/Zach.jpg'
                                            width={600}
                                            height={540}
                                        />
                                    </div>

                                    <div className="content">
                                        <h3>Zach McMordie</h3>
                                        <span>Designer</span>
                                    </div>
                                </div>
                            </div>

                            <div className="col-lg-3 col-md-6 col-sm-6 offset-lg-0 offset-md-3 offset-sm-3">
                                <div className="single-team-box">
                                    <div className="image">
                                        <Image
                                            alt=''
                                            src='/team/HelenLaird.jpg'
                                            width={600}
                                            height={540}
                                        />
                                    </div>

                                    <div className="content">
                                        <h3>Helen Laird</h3>
                                        <span>Junior Creative Copywriter</span>
                                    </div>
                                </div>
                            </div>

                            <div className="col-lg-3 col-md-6 col-sm-6 offset-lg-0 offset-md-3 offset-sm-3">
                                <div className="single-team-box">
                                    <div className="image">
                                        <Image
                                            alt=''
                                            src='/team/Nicole.jpg'
                                            width={600}
                                            height={540}
                                        />
                                    </div>

                                    <div className="content">
                                        <h3>Nicole Kelly</h3>
                                        <span>Digital Account Manager</span>
                                    </div>
                                </div>
                            </div>

                            <div className="col-lg-3 col-md-6 col-sm-6 offset-lg-0 offset-md-3 offset-sm-3">
                                <div className="single-team-box">
                                    <div className="image">
                                        <Image
                                            alt=''
                                            src='/team/Kerry.jpg'
                                            width={600}
                                            height={540}
                                        />
                                    </div>

                                    <div className="content">
                                        <h3>Kerry Gardner</h3>
                                        <span>Social Media Executive</span>
                                    </div>
                                </div>
                            </div>

                            <div className="col-lg-3 col-md-6 col-sm-6 offset-lg-0 offset-md-3 offset-sm-3">
                                <div className="single-team-box">
                                    <div className="image">
                                        <Image
                                            alt=''
                                            src='/team/Paddy.jpg'
                                            width={600}
                                            height={540}
                                        />
                                    </div>

                                    <div className="content">
                                        <h3>Paddy Murphy</h3>
                                        <span>Account Executive</span>
                                    </div>
                                </div>
                            </div>

                            <div className="col-lg-3 col-md-6 col-sm-6 offset-lg-0 offset-md-3 offset-sm-3">
                                <div className="single-team-box">
                                    <div className="image">
                                        <Image
                                            alt=''
                                            src='/team/Fintan.jpg'
                                            width={600}
                                            height={540}
                                        />
                                    </div>

                                    <div className="content">
                                        <h3>Fintan Kerr</h3>
                                        <span>Junior Designer</span>
                                    </div>
                                </div>
                            </div>

                            <div className="col-lg-3 col-md-6 col-sm-6 offset-lg-0 offset-md-3 offset-sm-3">
                                <div className="single-team-box">
                                    <div className="image">
                                        <Image
                                            alt=''
                                            src='/team/AoifeMcGivern.jpg'
                                            width={600}
                                            height={540}
                                        />
                                    </div>

                                    <div className="content">
                                        <h3>Aoife McGivern</h3>
                                        <span>Junior Designer</span>
                                    </div>
                                </div>
                            </div>


                            <div className="col-lg-3 col-md-6 col-sm-6 offset-lg-0 offset-md-3 offset-sm-3">
                                <div className="single-team-box">
                                    <div className="image">
                                        <Image
                                            alt=''
                                            src='/team/Dylan.jpg'
                                            width={600}
                                            height={540}
                                        />
                                    </div>

                                    <div className="content">
                                        <h3>Dylan McCrory</h3>
                                        <span>Marketing Assistant</span>
                                    </div>
                                </div>
                            </div>

                            <div className="col-lg-3 col-md-6 col-sm-6 offset-lg-0 offset-md-3 offset-sm-3">
                                <div className="single-team-box">
                                    <div className="image">
                                        <Image
                                            alt=''
                                            src='/team/Connor.jpg'
                                            width={600}
                                            height={540}
                                        />
                                    </div>

                                    <div className="content">
                                        <h3>Connor Mackey</h3>
                                        <span>Marketing Intern</span>
                                    </div>
                                </div>
                            </div>




                        </div>
                    </div>

                    {/* Animation shape image */}
                    <div className="shape-img2">
                        <img src="/images/shape/shape2.svg" alt="image" />
                    </div>
                    <div className="shape-img3">
                        <img src="/images/shape/shape3.svg" alt="image" />
                    </div>
                    <div className="shape-img4">
                        <img src="/images/shape/shape4.png" alt="image" />
                    </div>
                    <div className="shape-img5">
                        <img src="/images/shape/shape5.png" alt="image" />
                    </div>
                    <div className="shape-img6">
                        <img src="/images/shape/shape6.png" alt="image" />
                    </div>
                    <div className="shape-img9">
                        <img src="/images/shape/shape9.png" alt="image" />
                    </div>
                    <div className="shape-img10">
                        <img src="/images/shape/shape10.png" alt="image" />
                    </div>
                </section>
            </>
        );
    }
}

export default OurTeam;