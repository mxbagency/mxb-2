import React, { Component } from 'react';
import Image from 'next/image';

class SubscribeBoxedTwo extends Component {
    render() {
        return (
            <>
                <section className="subscribe-area pb-100">
                    <div className="container">
                        <div className="subscribe-inner-area jarallax">
                            <div className="subscribe-content">
                                <span className="sub-title">Join Our Newsletter</span>
                                <h2>Get the latest updates and tips from MXB straight to your inbox!</h2>

                                <form className="newsletter-form">
                                    <input type="email" className="input-newsletter" placeholder="Enter your email" name="email" />
                                    <button type="submit">Subscribe</button>
                                </form>
                            </div>
                        </div>
                    </div>

                    {/* Animation Shape Images */}
                    <div className="shape-img2">
                        <Image
                            alt=''
                            src='/shapes/shape2.svg'
                            width={30}
                            height={30}
                        />
                    </div>
                    <div className="shape-img3">
                        <Image
                            alt=''
                            src='/shapes/shape3.svg'
                            width={25}
                            height={25}
                        />
                    </div>
                    <div className="shape-img5">
                        <Image
                            alt=''
                            src='/shapes/shape5.png'
                            width={21}
                            height={21}
                        />
                    </div>
                    <div className="shape-img6">
                        <Image
                            alt=''
                            src='/shapes/shape7.png'
                            width={80}
                            height={80}
                        />
                    </div>
                </section>
            </>
        );
    }
}

export default SubscribeBoxedTwo;