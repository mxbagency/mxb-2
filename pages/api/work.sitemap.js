
import { SitemapStream, streamToPromise } from 'sitemap';

export default async (req, res) => {
    try {
        const smStream = new SitemapStream({
            hostname: `https://${req.headers.host}`,
            cacheTime: 600000,
        });

        // List of works
        const works = [];

        // Create each URL row
        works.forEach(work => {
            smStream.write({
                url: `/work/${work.slug}`,
                changefreq: 'monthly',
                priority: 0.9
            });
        });

        // End sitemap stream
        smStream.end();

        // XML sitemap string
        const sitemapOutput = (await streamToPromise(smStream)).toString();

        // Change headers
        res.writeHead(200, {
            'Content-Type': 'application/xml'
        });

        // Display output to user
        res.end(sitemapOutput);
    } catch (e) {
        console.log(e)
        res.send(JSON.stringify(e))
    }

}
