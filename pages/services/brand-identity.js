import React, { Component } from 'react';
import Navbar from '../../components/Layout/Navbar';
import PageHeader from '../../components/Common/PageHeader';
import ServiceDetailsContent from '../../components/ServiceDetails/ServiceDetailsContent';
import Footer from '../../components/Layout/Footer';

class BrandIdentity extends Component {
    render() {
        return (
            <>
                <Navbar />

                <PageHeader
                    pageTitle="Brand Identity"
                    breadcrumbTextOne="Services"
                    breadcrumbUrl="/services"
                    breadcrumbTextTwo="Brand Identity"
                />

                <ServiceDetailsContent />

                <Footer />
            </>
        );
    }
}

export default BrandIdentity;