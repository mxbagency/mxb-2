import React, { Component } from 'react';
import Navbar from '../components/Layout/Navbar';
import PageHeader from '../components/Common/PageHeader';
import ProjectsThreeGrid from '../components/Projects/ProjectsThreeGrid';
import Footer from '../components/Layout/Footer';

class Work extends Component {
    render() {
        return (
            <>
                <Navbar />

                <PageHeader
                    pageTitle="Our Work"
                    breadcrumbTextOne="Home"
                    breadcrumbUrl="/"
                    breadcrumbTextTwo="Our Work"
                    height="400px"
                />

                <ProjectsThreeGrid />

                <Footer />
            </>
        );
    }
}

export default Work;