import React, { Component } from 'react';
import Navbar from '../components/Layout/Navbar';
import PageHeader from '../components/Common/PageHeader';
import AboutArea from '../components/About/AboutArea';
import FunFacts from '../components/Common/FunFacts';
import WhyChooseUs from '../components/About/WhyChooseUs';
import Solution from '../components/Common/Solution';
import Testimonials from '../components/Common/Testimonials';
import OurTeam from '../components/Common/OurTeam';
import SubscribeBoxedTwo from '../components/Common/SubscribeBoxedTwo';
import PartnerSlider from '../components/Common/Partner/PartnerSlider';
import Footer from '../components/Layout/Footer';
import FunFactsTwo from '../components/Common/FunFactsTwo';

class About extends Component {
    render() {
        return (
            <>
                <Navbar />
                <PageHeader
                    pageTitle="About Us"
                    breadcrumbTextOne="Home"
                    breadcrumbUrl="/"
                    breadcrumbTextTwo="About Us"
                    height="700px"
                />
                <AboutArea />
                <FunFactsTwo />
                <WhyChooseUs />
                <Solution />
                {/* <Testimonials /> */}
                <OurTeam />

                <PartnerSlider />
                <SubscribeBoxedTwo />
                <Footer />
            </>
        );
    }
}

export default About;