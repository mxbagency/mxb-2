import React, { Component } from 'react';
import Navbar from '../components/Layout/Navbar';
import PageHeader from '../components/Common/PageHeader';
import CareersThreeGrid from '../components/Careers/CareersThreeGrid';
import Footer from '../components/Layout/Footer';

class Careers extends Component {
    render() {
        return (
            <>
                <Navbar />

                <PageHeader
                    pageTitle="Careers"
                    breadcrumbTextOne="Home"
                    breadcrumbUrl="/"
                    breadcrumbTextTwo="Careers"
                    height="400px"
                />

                <CareersThreeGrid />

                <Footer />
            </>
        );
    }
}

export default Careers;