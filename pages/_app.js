import '../assets/css/bootstrap.min.css';
import '../assets/css/fontawesome.min.css';
import '../assets/css/animate.min.css';
import '../assets/css/flaticon.css';
import '../assets/css/boxicons.min.css';
import 'react-toastify/dist/ReactToastify.css';
import '../node_modules/react-modal-video/scss/modal-video.scss';
import 'react-accessible-accordion/dist/fancy-example.css';
import 'react-image-lightbox/style.css';
import '../node_modules/react-modal-video/css/modal-video.min.css';

import '../assets/css/style.scss';
import '../assets/css/responsive.scss';

import { Provider } from 'react-redux';
import App from 'next/app';
import Head from 'next/head';
import withRedux from 'next-redux-wrapper';
import { initStore } from '../store/reducers/cartReducer';
import Loader from '../components/Shared/Loader';
import GoTop from '../components/Shared/GoTop';
import { AnimateSharedLayout } from "framer-motion";

export default withRedux(initStore)(
    class MyApp extends App {

        // Preloader
        state = {
            loading: true
        };
        componentDidMount() {
            this.timerHandle = setTimeout(() => this.setState({ loading: false }), 2000);
        }
        componentWillUnmount() {
            if (this.timerHandle) {
                clearTimeout(this.timerHandle);
                this.timerHandle = 0;
            }
        }

        render() {
            const { Component, pageProps, store } = this.props

            return (
                <>
                    <Head>
                        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
                        <title>MXB Agency - Digital Marketing Belfast</title>

                        <link rel="apple-touch-icon" sizes="180x180" href="/favicons/apple-touch-icon.png" />
                        <link rel="icon" type="image/png" sizes="32x32" href="/favicons/favicon-32x32.png" />
                        <link rel="icon" type="image/png" sizes="194x194" href="/favicons/favicon-194x194.png" />
                        <link rel="icon" type="image/png" sizes="192x192" href="/favicons/android-chrome-192x192.png" />
                        <link rel="icon" type="image/png" sizes="16x16" href="/favicons/favicon-16x16.png" />
                        <link rel="manifest" href="/favicons/site.webmanifest" />
                        <link rel="mask-icon" href="/favicons/safari-pinned-tab.svg" color="#6e4aff" />
                        <meta name="msapplication-TileColor" content="#6e4aff" />
                        <meta name="theme-color" content="#ffffff" />
                    </Head>

                    <Provider store={store}>
                        <AnimateSharedLayout>
                            <Component {...pageProps} />
                        </AnimateSharedLayout>
                    </Provider>

                    {/* Preloader */}
                    <Loader loading={this.state.loading} />

                    {/* Go Top Button */}
                    <GoTop scrollStepInPx="50" delayInMs="16.66" />
                </>
            );
        }
    }
)